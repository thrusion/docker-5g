# RELEASE NOTES: #

## v1.1.0 -- July 2021 ##

* FQDN DNS resolution
* Bug fixes
* Full support for Ubuntu18 and RHEL8

## v1.0.0 -- Jan 2021 ##

* Initial release

