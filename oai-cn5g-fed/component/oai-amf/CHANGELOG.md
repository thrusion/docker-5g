# RELEASE NOTES: #

## v1.1.0 -- July 2021 ##

* Session Release
* NRF registration
  - with FQDN DNS resolution
* Multiple PDU support
* Bug fixes
* Full support for Ubuntu18 and RHEL8

## v1.0.0 -- September 2020 ##

* Initial release

