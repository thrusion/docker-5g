docker build --target oai-amf --tag oai-amf:latest \
               --file component/oai-amf/docker/Dockerfile.amf.ubuntu18 \
               component/oai-amf

docker build --target oai-smf --tag oai-smf:latest \
               --file component/oai-smf/docker/Dockerfile.smf.ubuntu18 \
               component/oai-smf

docker build --target oai-nrf --tag oai-nrf:latest \
               --file component/oai-nrf/docker/Dockerfile.nrf.ubuntu18 \
               component/oai-nrf

docker build --target oai-spgwu-tiny --tag oai-spgwu-tiny:latest \
               --file component/oai-upf-equivalent/docker/Dockerfile.ubuntu18.04 \
               component/oai-upf-equivalent


docker build --target oai-ausf --tag oai-ausf:latest \
               --file component/oai-ausf/docker/Dockerfile.ausf.ubuntu18 \
               component/oai-ausf

docker build --target oai-udm --tag oai-udm:latest \
               --file component/oai-udm/docker/Dockerfile.udm.ubuntu18 \
               component/oai-udm

docker build --target oai-udr --tag oai-udr:latest \
               --file component/oai-udr/docker/Dockerfile.udr.ubuntu18 \
               component/oai-udr

docker image prune --force
